#include <iostream>
#include "Templates.h"
//#include "Memory.h"
#include "Language.h"
#include "Trace.h"
#include "SmartPointers.h"

using namespace std;

int main(int argc, char* argv[])
{
    //cout << argv[0] << endl;
    //consuming_templates();
    //memoryFundas();
    //arrayFundas();
    //copyConstructorFunda();
    //LambdasFunda();
    //iteratorFundas();
    //traceFunda();
    smartPointerFundas();
    return 0;
}