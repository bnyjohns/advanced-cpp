#pragma once
#include <array>
#include <iostream>
#include <cstdint>
#include <vector>
#include <cassert>

using namespace std;

void arrayFundas()
{
	int simpleArray[] { 1,2,3 }; //no length specified
	int betterArray[10]{ 0 }; //length specified and first element is 10
	array<int, 3> a { 1,2,3 }; //using array type in standard library
	auto b = new array<int, 2> { 1, 2 }; //on heap

	//auto* p = simpleArray;
	for (int i = 0; i < 3; i++)
	{
		cout << *(simpleArray + i) << endl;
	}
}

void stringFundas()
{
	const char *hello = "hello";
	string hi { "hi" };
	auto length = hi.length();
}

void integralTypes()
{
	int a; // a is a signed integer. Size is platform specific. Default value is unknown
	size_t size1 = sizeof(int);
	size_t size2 = sizeof(a);

	int32_t b(1); //signed 32 bit type
}

namespace life
{
	auto meaning = 42;
	auto* mptr = &meaning; //* needed with auto for pointers
	auto& mref = meaning; //& needed with auto for references
}

void namespaceFunda()
{
	using life::meaning; //only meaning gets imported from life namespace
	auto temp = meaning;
}

void stack_and_heap()
{
	string hi("hi");
	string *hello = new string("hello");
	delete hello;

	int* values = new int[128];
	delete[] values;
}

int add(int a, int b) { return a + b; }
auto add2(int a, int b) -> int { return a + b; }


void LambdasFunda()
{
	int x = 1;
	auto add2 = [](int a) { return a + 2; };
	cout << add2(1) << endl;

	int by = 10;
	auto IncreaseByValue = [by](int a) { return a + by; }; //'by' passed by value
	auto IncreaseByReference = [&by](int a) { return a + by; }; //'by' passed by reference
	by = 100;

	cout << IncreaseByValue(2) << endl;
	cout << IncreaseByReference(2) << endl;
}

enum Color //in global scope
{
	Red,
	Green
};

enum class GameState //in GameState namespace
{
	Win,
	Lose
};

void enumFundas()
{
	Color c = Red;
	int i = c;

	GameState g = GameState::Win;
	//int j = g; //won't work
}

union Data //all the fields occupy the same memory (4 bytes reused by the 3 fields)
{
	int integer; //4 bytes
	float fpNumber; // 4 bytes
	char* text;
};

void unionFundas()
{
	Data d;
	d.fpNumber = 0.5f;
}

struct Size //plain old data container
{
	int width, height;
};

class Address 
{	
public:
	string city;
	Address(string city):city(city){}
};

class Employee
{
	string name;	
public:
	Address* address = nullptr;
	Employee(string name, string city): name(name)
	{
		address = new Address(city);
	}
	Employee(const Employee& e) : name(e.name)
	{
		address = new Address(e.address->city);
	}
	~Employee()
	{
		if(address != nullptr)
		{
			delete address;
			address = nullptr;
		}		
	}
};

void copyConstructorFunda()
{
	auto emp1 = new Employee("Raj", "Chennai");
	auto emp2 = new Employee(*emp1);
	emp2->address->city = "Rome";
	cout << "emp1 city: " << emp1->address->city << endl;
	delete emp1;
	delete emp2;
}


void iteratorFundas()
{
	int a[]{ 1,2,3,4 };
	for (int i = 0; i < 4; i++)
		cout << a[i] << "\t";
	cout << endl;

	for (int* p = a, *e = a + 4; p != e; p++) //pointer arithmetic. (a+4 will move pointer by 16 bytes) (p++ will move pointer by 4 bytes)
		cout << *p << "\t";
	cout << endl; 

	auto ba = begin(a);
	auto ea = end(a);
	for (; ba != ea; ba++)
		cout << *ba << "\t";
	cout << endl;

	for(auto value: a)
		cout << value << "\t";
	cout << endl;

	vector<int> v{ 1, 2, 3, 4};
	auto bv = begin(v); //v.begin() also will work
	auto cbv = cbegin(v);
	//*cbv = 2; //won't work since cbv is const iterator

	auto p = v.rbegin(); //beginning in reverse
	for (auto a = v.rbegin(); a != v.rend(); a++)
		cout << *a << "\t";
	cout << endl;
}

//Assertions
auto assertions_Verify() -> void
{
	//Macros don't own namespaces
	//assert works only in debug mode for runtime
	assert(4 == 5); //assert in smaller case since inherited from C
	static_assert(sizeof(float) == 4, "not right"); //compile time asserts
}
