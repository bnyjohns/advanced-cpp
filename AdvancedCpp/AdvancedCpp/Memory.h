#pragma once
#include <string>
#include <iostream>
#include <memory>

using namespace std;

class Address
{
public:
	string city;
	Address()
	{

	}
	Address(const string& city) : city(city)
	{
		cout << "Address with city: " << city << " created" << endl;
	}
	~Address()
	{
		cout << "Address with city: " << city << " destroyed" << endl;
	}
};

class Person
{
public:
	Address* address = nullptr;
	Person(string city)
	{
		address = new Address(city);
		cout << "Person created" << endl;
	}
	~Person()
	{
		delete address;
		cout << "Person destroyed" << endl;
	}
};

class Person2
{
	Person2(){}
	Person2(const Person2& _){}
	~Person2(){}
	int ref_count = 0;
public:
	static Person2* create()
	{
		auto p = new Person2();
		p->ref_count = 1;
		return p;
	}
	void add_ref() { ref_count++; }
	void release()
	{
		if (--ref_count == 0)
			delete this;
	}
};

void inc(int& x) { x++; }
int meaningOfLife() { return 42; }
void move_rvalue()
{
	int a = 0;
	inc(a);

	//below lines will fail since they are rvalues
	//inc(1);
	//int& x = meaningOfLife();
	int&& x = meaningOfLife();
	cout << x << endl;

	Address book[100];
	// 2 instances get allocated here. One the actual instance and one in array
	//book[55] = Address("Rome"); 
	book[55] = move(Address("Rome"));

	auto swap = [](int& a, int& b)
	{
		auto temp = a; //copy happens here
		a = b;
		b = temp;
	};
	auto betterSwap = [](int& a, int& b)
	{
		int tmp(move(a));
		a = move(b);
		b = move(tmp);
	};
	int p = 2, q = 3;
	swap(p, q);
}

unique_ptr<Address> create_address(string const& city)
{
	//auto* a = new Address(city);
	//a->city = "New " + city;
	//return unique_ptr<Address>(a);
	return make_unique<Address>(city);
}

class Person3
{
public:
	shared_ptr<Address> address;
	Person3(string const& city)
	{
		address = make_shared<Address>(city);
		cout << "Created person" << endl;
	}
	~Person3()
	{
		//nothing here. Smart pointer take care of deallocating address
		cout << "Destroyed person" << endl;
	}
};

void memoryFundas()
{
	/*Address* a;
	{
		Person p("Rome");
		a = p.address;
	}*/
	//cout << a->city << endl; //this will throw exception since at this point "p" is already out of scope and "a" gets destroyed

	auto address = create_address("Paris");
	//auto v(address); //copy constructor won't work
	//auto w = address; //copy assignment also won't work on unique pointers

	shared_ptr<Address> a;
	{
		Person3 p("Rome");
		a = p.address;
	}
	cout << a->city << endl; //will work since reference counted


}
