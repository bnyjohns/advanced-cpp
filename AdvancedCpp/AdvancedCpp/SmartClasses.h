#pragma once
#include <winnt.h>
#include <cassert>

namespace Bunny
{
	template<typename Traits>
	class unique_handle
	{
		typedef typename Traits::pointer pointer;

		pointer m_value;
		void close()
		{

		}
	public:
		unique_handle()
		{

		}
	};

	struct null_handle_traits
	{
		typedef HANDLE pointer;

		static auto invalid() throw() -> pointer
		{
			return nullptr;
		}

		static void close(pointer value)
		{
			assert(value);
		}
	};

	typedef unique_handle<null_handle_traits> null_handle;
}

using namespace Bunny;

void smartClassDemo()
{
	auto h = null_handle{};
}
