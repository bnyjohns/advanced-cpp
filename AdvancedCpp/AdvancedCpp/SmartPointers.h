#pragma once
#include <memory>
#include <iostream>
#include <cassert>

using namespace std;
class Hen
{
public:
	unsigned id;
	float eggs;
	Hen(unsigned id, float eggs) : id(id), eggs(eggs)
	{

	}
public:
	~Hen() 
	{ 
		cout << "Hen destroyed" << endl;
	}
};

auto getHen() -> unique_ptr<Hen>
{
	return make_unique<Hen>(2, 3.0f);
}

auto updateHen(unique_ptr<Hen> hen) -> unique_ptr<Hen>
{
	hen->eggs += 1.8f;
	return hen;
}

void uniquePtrDemo()
{
	auto hen = make_unique<Hen>(1, 2.0);
	//auto hen2 = hen; //won't work
	auto hen3 = move(hen); //will work. Ownership changed to hen3

	Hen copy = *hen3;
	Hen& ref = *hen3;
	Hen* ptr = hen3.release();
	hen3.reset(ptr);

	auto hen4 = getHen(); //nothing to release memory of
	//updateHen(hen4); //unique ptr can't be copied and hence won't work
	hen4 = updateHen(move(hen4)); //will work
}

//reference counted smart pointer
//share pointer points to 2 items. One to the object managed and 1 to the reference count
void sharedPointerDemo()
{
	auto sp = shared_ptr<int>{};
	assert(!sp);
	assert(sp.use_count() == 0);
	assert(!sp.unique()); //verifies use_count is 1
		
	sp = make_shared<int>(2);
	assert(sp);
	assert(sp.use_count() == 1);
	assert(sp.unique()); //verifies use_count is 1	

	auto sp2 = sp;
	assert(sp.use_count() == 2);
	assert(!sp.unique()); 

	assert(sp2.use_count() == 2);
	assert(!sp2.unique());

	int copy = *sp;
	int& ref = *sp;
	int* ptr = sp.get(); //raw pointer

	assert(sp.get() == sp2.get());
	assert(sp == sp2); //equality operator
}

//weak pointer has 2 reference counts: weak reference and strong reference
void weakPointerDemo()
{
	auto sp = make_shared<int>(123);
	auto wp = weak_ptr<int>{ sp };

	assert(!wp.expired());
	assert(wp.use_count() == 1); //returns strong reference count

	if (auto lock = wp.lock()) //returns a shared pointer to the strong reference
	{
		cout << "locked! " << *lock << endl;
	}
	sp = nullptr;
	
	assert(wp.expired());
	assert(wp.use_count() == 0);

	if (auto lock = wp.lock())
		cout << "locked!" << *lock << endl;
	else
		wp.reset(); //to release control block (even though it doesn't hold a strong reference)
}

void smartPointerFundas()
{
	uniquePtrDemo();
	sharedPointerDemo();
	weakPointerDemo();
}
