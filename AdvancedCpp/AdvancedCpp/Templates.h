#pragma once
#include <tuple>
#include <complex>
#include <iostream>

using namespace std;

//typedef tuple<int, int, int> trie1;

pair<int, int> sumAndPair(int a, int b)
{
	auto sum = a + b;
	auto product = a * b;
	return pair<int, int>(sum, product);
};

//trie1 sum_product_avg(int a, int b)
//{
//	trie1 t { (a + b), (a * b), (a + b) / 2 };
//	return t;
//}

//Template classes
template<typename T1, typename T2, typename T3>
struct triple
{
	typedef T1 result_type;

	T1 first;
	T2 second;
	T3 third;
	triple(const T1& first, const T2& second, const T3& third) :
		first(first), second(second), third(third)
	{

	}
};
typedef triple<int, int, int> trie2;

trie2 sum_product_avg(int a, int b)
{
	trie2 t { (a + b), (a * b), (a + b) / 2 };
	return t;
};

//Template functions
template<typename T1, typename T2, typename T3>
triple<T1, T2, T3> sum_product_average(const T1& a, const T2& b, const T3& c)
{
	return triple<T1, T2, T3>(a + b + c, a* b* c, 
		(a + b + c) / triple<T1,T2,T3>::result_type(3));
};

typedef complex<double> cd;
typedef triple<cd, cd, cd> cdt;

//template specialized for cd
template<> cdt sum_product_average(const cd& a, const cd& b, const cd& c)
{
	auto result = sum_product_average(a.real(), b.real(), c.real());
	return cdt(result.first, result.second, result.third);
}

void template_specialization()
{
	cd a(2, 3), b(3, 4), c(4,5);
	auto result = sum_product_average(a, b, c);
	cout << "sum is " << result.first << endl;
	cout << "product is " << result.second << endl;
	cout << "average is " << result.third << endl;
}

template<typename T> //specialized function in case only 1 parameter passed in sum method
T sum(T t)
{
	return t;
}

template<typename T, typename... U>
auto sum(T t, U... u) -> decltype(t) //decltype for indicating the return type of the 'sum' method
{
	return t + sum(u...);
}

void variadic() 
{
	cout << "sum is: " << sum(1,2,3,4) << endl;
	cout << sum(string("no "), string("time "), string("to "), string("die")) << endl;
}

template <int n>
struct Factorial
{
	enum { value = n * Factorial<n-1>::value };
};

template <>
struct Factorial <0>
{
	enum { value = 1 };
};

//makes use of compile time availability of template details
void metaprogramming()
{
	int x = Factorial<4>::value;
	int y = Factorial<0>::value;
}


void consuming_templates()
{
	auto result1 = sumAndPair(2, 3);
	cout << "sum is " << result1.first << endl;
	cout << "product is " << result1.second << endl;

	/*auto result2 = sum_product_avg(2, 3);
	auto sum2 = get<0>(result2);
	auto avg2 = get<2>(result2);*/

	auto result3 = sum_product_avg(2, 3);
	auto sum3 = result3.first;
	auto prod3 = result3.second;
	auto avg3 = result3.third;

	auto result4 = sum_product_average(14, 2.0, 3.5f);
	template_specialization();
	variadic();
	metaprogramming();
}

