#pragma once
//cout, outputDebugString, Event Tracing
#include <stdio.h>
#include <Windows.h>
#include <cassert>


inline auto Trace(wchar_t const* format, ...) -> void
{
	va_list args;
	va_start(args, format);

	wchar_t buffer[256];
	assert(-1 != _vsnwprintf_s(buffer, _countof(buffer) - 1, format, args));

	va_end(args);
	OutputDebugString(buffer);
}

#ifdef _DEBUG
template <typename... Args>
struct Tracer
{
	char const* fileName;
	unsigned const line;
	
	Tracer(char const* fileName, unsigned const line) : 
		fileName(fileName), line(line)
	{

	}

	template <typename... Args>
	auto operator()(wchar_t const* format, Args... args) const -> void
	{
		wchar_t buffer[256];
		auto count = swprintf_s(buffer, L"%S(%d): ", fileName, line);
		assert(-1 != count);

		assert(-1 != _snwprintf_s(buffer + count, 
						_countof(buffer) - count,
						_countof(buffer) - count - 1,
						format,
						args...));

		OutputDebugString(buffer);
	}
};
#endif

#ifdef _DEBUG
#define TRACE Tracer<int>(__FILE__, __LINE__)
#else
#define TRACE __noop
#endif

auto traceFunda()
{
	auto s = L"1 + 2 = %d\n";
	TRACE(s, 3);
}
